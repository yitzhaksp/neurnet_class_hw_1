import numpy as np
from numpy import linalg as LA
from time import sleep
from misc_utils import *
import os


n_patches=175
suf='plain'
mse_file='logs/mse_'+suf+'.log'
partition=[]
for i in range(n_patches):
    patch=np.load('data/partitions/text/patch_'+str(i)+'.npy' )
    partition.append(patch.flatten())
#net_dim=[256,50,256]
encoder_weigts=np.load('models/encoder_weights_'+suf+'.npy')
decoder_weigts=np.load('models/decoder_weights_'+suf+'.npy')
net_weights=[encoder_weigts , decoder_weigts ]
mse=0
for i,patch in enumerate(partition):
    net_output=forwardprop(patch, net_weights)
    mse+= LA.norm(net_output[2] - patch)
mse/=len(partition)
print(" mse={}".format(mse))


tmp=7