import numpy as np

def summ(lst1,c,lst2):
    y=[]
    for w1,w2 in zip(lst1,lst2):
        y.append(w1+c*w2)
    return y

#RELU unit
def f(x):
    if x>0:
        return x
    else:
        return 0.0

# derivative of RELU unit
def df_dx(x):
    if x>0:
        return 1.0
    else:
        return 0.0

def forwardprop(x,net_weights):
    out_all=[x]
    out_prev=x
    for l in range(len(net_weights)):
        inp_this=np.dot(net_weights[l],out_prev)
        out_this=np.empty_like(inp_this)
        for i in range(inp_this.size):
            out_this[i]=f(inp_this[i])
        out_all.append( out_this )
        out_prev=out_this
    return out_all



def backprop(deltas_nxt,w,inp):
    dE_doutp=np.dot(np.transpose(w),deltas_nxt)
    deltas_this=np.zeros(w.shape[1])
    for i in range(w.shape[1]):
        deltas_this[i]=df_dx(inp[i])*dE_doutp[i]
    return deltas_this


def comp_deltas(net_weights, out, t):
    inp_last=np.dot(net_weights[-1], out[-2])
    deltas_nxt=np.empty_like(out[-1])
    for i in range(deltas_nxt.size):
        deltas_nxt[i]=2* df_dx(inp_last[i]) * (out[-1][i]- t[i])
    deltas=[deltas_nxt]
    num_layers=len(net_weights)
    for l in range(num_layers-1,0,-1):
        inp=np.dot(net_weights[l], out[l])
        deltas_this=backprop(deltas_nxt, net_weights[l],inp)
        deltas.append(deltas_this)
        deltas_nxt=deltas_this
    deltas.reverse()
    return deltas


def comp_dE_dw(net_weights,out,deltas):
    dE_dw=[]
    for l in range(len(net_weights)):
        # w_ij is the weight between output_j and neuron_i
        #grad[i,j]=out[l][j]*deltas[l][i]
        grad=np.expand_dims(deltas[l],1) * np.expand_dims(out[l],0)
        dE_dw.append(grad)
    return dE_dw


