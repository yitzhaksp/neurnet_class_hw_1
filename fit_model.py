import numpy as np
from numpy import linalg as LA
from time import sleep
from misc_utils import *
import os

suf='highcompr'
n_patches=200
mse_file='logs/mse_'+suf+'.log'
patches=[]
for i in range(n_patches):
    patch=np.load('data/image_patches/Lena/patch_'+str(i)+'.npy' )
    patches.append(patch.flatten())
net_dim=[256,25,256]
net_weights=[np.random.randn(net_dim[1],net_dim[0]) , np.random.randn(net_dim[2],net_dim[1]) ]
#x=np.random.rand(net_dim[0])
alpha=.002
i=0
n_epochs=500
epoch=0
mse_log = open(mse_file, "w"); mse_log.close()

for epoch in range(n_epochs):
    if i>=len(patches):
        i=0
    out = forwardprop(patches[i], net_weights)
    trg=patches[i]
    deltas = comp_deltas(net_weights, out, trg)
    dE_dw = comp_dE_dw(net_weights, out, deltas)
    net_weights=summ(net_weights,-alpha,dE_dw)
    net_outputs, mse=[], 0.0
    for patch in patches:
        net_output=forwardprop(patch, net_weights)
        mse+= LA.norm(net_output[2] - patch)
    mse/=len(patches)
    print("epoch={}, mse={}".format(epoch,mse))
    mse_log = open(mse_file, "a")
    mse_log.write(str(mse) + "\n")
    mse_log.close()
    #sleep(.5)
    i+=1

np.save('models/encoder_weights_'+suf+'.npy',net_weights[0])
np.save('models/decoder_weights_'+suf+'.npy',net_weights[1])



tmp=7