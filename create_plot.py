import numpy as np
import matplotlib.pyplot as plt


with open('logs/mse_plain.log', 'r') as fp:
    losses = fp.read().splitlines()
losses_num = [float(x) for x in losses]
plt.plot(losses_num[:500])
plt.grid(True)
plt.xlabel('epoch')
plt.ylabel('loss')
plt.yscale('log')
plt.show()

tmp=7