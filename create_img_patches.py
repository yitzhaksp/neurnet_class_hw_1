import numpy as np
from PIL import Image as pil_image
img_name='text'
img = np.array(pil_image.open('data/'+img_name+'.jpg').convert('L'))
patch_size=16
partition=True
if partition:
    x1_min,i=0,0
    while (x1_min+patch_size) < img.shape[0]:
        x2_min=0
        while (x2_min+patch_size) < img.shape[1]:
            print(i)
            patch = img[x1_min: x1_min + patch_size, x2_min: x2_min + patch_size]
            patch = patch.astype(np.float16) / 256.0
            np.save('data/partitions/'+img_name+'/patch_' + str(i) + '.npy', patch)
            x2_min += patch_size
            i+=1
        x1_min+= patch_size


else:
    for i in range(1000):
        x1_min, x2_min= np.random.randint(240),np.random.randint(240)
        patch=img[x1_min: x1_min+ patch_size , x2_min: x2_min+ patch_size]
        patch=patch.astype(np.float16)/256.0
        np.save('data/image_patches/'+img_name+'/patch_'+str(i)+'.npy',patch)
        tmp=7